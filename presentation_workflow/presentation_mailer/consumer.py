import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(dictionary):
    send_mail(
        "Your presentation has been accepted",
        f"{dictionary['name']}, We're happy to tell you that your presentation {dictionary['title']} has been accepted",
        "admin@conference.go",
        [dictionary["email"]],
        fail_silently=False,
    )


def process_rejection(dictionary):
    send_mail(
        "Your presentation has been rejected",
        f"{dictionary['name']}, We're sorry to tell you that your presentation {dictionary['title']} has been rejected",
        "admin@conference.go",
        [dictionary["email"]],
        fail_silently=False,
    )


def process_message(cd, method, properties, body):
    dictionary = json.loads(body)
    status = dictionary["status"]
    if status == "approved":
        process_approval(dictionary)
    if status == "rejected":
        process_rejection(dictionary)


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="tasks")
        channel.basic_consume(
            queue="tasks",
            on_message_callback=process_message,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ. Retrying in a few seconds...")
        time.sleep(2)
